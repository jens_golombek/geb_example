package at.golombek.presentation.geb.tests

import at.golombek.presentation.geb.pages.LoginPage1
import at.golombek.presentation.geb.pages.SettingsPage
import geb.spock.GebReportingSpec

/**
 * Created by jens on 05.09.14.
 */
class TestChangeColor extends GebReportingSpec {

    def "testChangeColorToYellow"()  {
        given:
        to LoginPage1
        loginForm.username = "user1"
        loginForm.password = "user1"
        loginForm.loginButton.click()
        to (SettingsPage)

        when:
        //Composition
        changeSkin('orange').click()

        then:
        true


    }
}
