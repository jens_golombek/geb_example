package at.golombek.presentation.geb.tests

import at.golombek.presentation.geb.pages.HomePage
import at.golombek.presentation.geb.pages.LoginPage
import geb.spock.GebReportingSpec

/**
 * Created by jens on 03.09.14.
 */
class TestLogin extends GebReportingSpec{

    def "can login"() {

        given:
        to LoginPage

        when:
        loginForm.with {
            j_username = "user1"
            j_password = "user1"
        }

        and:
        loginButton.click()

        then:
        at HomePage

    }
}
