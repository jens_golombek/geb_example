package at.golombek.presentation.geb.tests

import at.golombek.presentation.geb.pages.HomePage
import at.golombek.presentation.geb.pages.LoginPage
import at.golombek.presentation.geb.pages.LoginPage1
import geb.spock.GebReportingSpec

/**
 * Created by jens on 09.09.14.
 */
class TestLogin2 extends GebReportingSpec {
    def "can login"() {

        given:
        to LoginPage1

        when:
        loginForm.username = "user1"
        loginForm.password = "user1"
        loginForm.loginButton.click()

        then:
        at HomePage

    }
}
