package at.golombek.presentation.geb.pages

import geb.Page

/**
 * Created by jens on 09.09.14.
 */
class LoginPage1 extends Page {

    static content = {
        loginForm { module LoginForm }
    }

}
