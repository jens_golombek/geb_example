package at.golombek.presentation.geb.pages

import geb.Page

/**
 * Created by jens on 03.09.14.
 */
class LoginPage extends Page {

    static at = { title.trim() == "Login ScrumToys" }

    static content = {
        loginForm  { $("form") }
        loginButton { loginForm.submit() }
    }

}

