package at.golombek.presentation.geb.pages

import geb.Page

/**
 * Created by jens on 03.09.14.
 */
class HomePage extends Page {

    static at = { title.trim() == "JSF 2.0 Demo - Scrum Whiteboard Application" }

    static content = {
        menutable  { $("#menuTable")}

        //Menu Entries
        changeSkin { $(".menu a", href: contains("changeSkin")) }
    }
}
