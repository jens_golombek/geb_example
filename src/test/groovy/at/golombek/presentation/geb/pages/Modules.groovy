package at.golombek.presentation.geb.pages

import geb.Module

/**
 * Created by jens on 09.09.14.
 */
class LoginForm extends Module {

        static content = {
            loginForm { $("form") }
            loginButton { loginForm.submit() }
            username { $("#username") }
            password { $("#password") }
        }
}
