package at.golombek.presentation.geb.pages

import geb.Page

/**
 * Created by jens on 05.09.14.
 */
class SettingsPage extends Page {

   static url = "http://localhost:8080/scrumtoys/skin/changeSkin.jsf"

    static content = {
       changeSkin { color -> $("a", text: color) }
    }
}
