package at.golombek.presentation.geb;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Simple Example which shows the usage of WebDriver
 * Created by GOLO008 on 09.09.2014.
 */
public class Search {

    public static void main (String[] args ) {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.de");
        WebElement element = driver.findElement(By.id("gbqfq"));
        element.sendKeys("Gebish");
    }
}
